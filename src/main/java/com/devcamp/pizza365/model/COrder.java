package com.devcamp.pizza365.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Orders")
public class COrder {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "kich_co")
	private String kickCo;

	@Column(name = "duong_kinh")
	private String duongKinh;

	@Column(name = "suon")
	private int suon;

	@Column(name = "salad")
	private String salad;

	@Column(name = "loai_pizza")
	private String loaiPizza;

	@Column(name = "id_voucher")
	private String idVourcher;

	@Column(name = "thanh_tien")
	private int thanhtien;

	@Column(name = "giam_gia")
	private int giamGia;

	@Column(name = "id_loai_nuoc_uong")
	private String idLoaiNuocUong;

	@Column(name = "so_luong_nuoc")
	private String soLuongNuoc;

	@Column(name = "ho_ten")
	private String hoTen;

	@Column(name = "email")
	private String email;

	@Column(name = "so_dien_thoai")
	private String soDienThoai;

	@Column(name = "dia_chi")
	private String diaChi;

	@Column(name = "loi_nhan")
	private String loiNhan;

	@Column(name = "trang_thai")
	private String trangThai;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ngay_tao", nullable = true, updatable = false)
	@CreatedDate
	@CreationTimestamp
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date ngayTao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ngay_cap_nhat", nullable = true)
	@LastModifiedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date ngayCapNhat;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKickCo() {
		return kickCo;
	}

	public void setKickCo(String kickCo) {
		this.kickCo = kickCo;
	}

	public String getDuongKinh() {
		return duongKinh;
	}

	public void setDuongKinh(String duongKinh) {
		this.duongKinh = duongKinh;
	}

	public int getSuon() {
		return suon;
	}

	public void setSuon(int suon) {
		this.suon = suon;
	}

	public String getSalad() {
		return salad;
	}

	public void setSalad(String salad) {
		this.salad = salad;
	}

	public String getLoaiPizza() {
		return loaiPizza;
	}

	public void setLoaiPizza(String loaiPizza) {
		this.loaiPizza = loaiPizza;
	}

	public String getIdVourcher() {
		return idVourcher;
	}

	public void setIdVourcher(String idVourcher) {
		this.idVourcher = idVourcher;
	}

	public int getThanhtien() {
		return thanhtien;
	}

	public void setThanhtien(int thanhtien) {
		this.thanhtien = thanhtien;
	}

	public int getGiamGia() {
		return giamGia;
	}

	public void setGiamGia(int giamGia) {
		this.giamGia = giamGia;
	}

	public String getIdLoaiNuocUong() {
		return idLoaiNuocUong;
	}

	public void setIdLoaiNuocUong(String idLoaiNuocUong) {
		this.idLoaiNuocUong = idLoaiNuocUong;
	}

	public String getSoLuongNuoc() {
		return soLuongNuoc;
	}

	public void setSoLuongNuoc(String soLuongNuoc) {
		this.soLuongNuoc = soLuongNuoc;
	}

	public String getHoTen() {
		return hoTen;
	}

	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSoDienThoai() {
		return soDienThoai;
	}

	public void setSoDienThoai(String soDienThoai) {
		this.soDienThoai = soDienThoai;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public String getLoiNhan() {
		return loiNhan;
	}

	public void setLoiNhan(String loiNhan) {
		this.loiNhan = loiNhan;
	}

	public String getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}

	public Date getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Date ngayTao) {
		this.ngayTao = ngayTao;
	}

	public Date getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Date ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	public COrder(int id, String kickCo, String duongKinh, int suon, String salad, String loaiPizza, String idVourcher,
			int thanhtien, int giamGia, String idLoaiNuocUong, String soLuongNuoc, String hoTen, String email,
			String soDienThoai, String diaChi, String loiNhan, String trangThai, Date ngayTao, Date ngayCapNhat) {
		super();
		this.id = id;
		this.kickCo = kickCo;
		this.duongKinh = duongKinh;
		this.suon = suon;
		this.salad = salad;
		this.loaiPizza = loaiPizza;
		this.idVourcher = idVourcher;
		this.thanhtien = thanhtien;
		this.giamGia = giamGia;
		this.idLoaiNuocUong = idLoaiNuocUong;
		this.soLuongNuoc = soLuongNuoc;
		this.hoTen = hoTen;
		this.email = email;
		this.soDienThoai = soDienThoai;
		this.diaChi = diaChi;
		this.loiNhan = loiNhan;
		this.trangThai = trangThai;
		this.ngayTao = ngayTao;
		this.ngayCapNhat = ngayCapNhat;
	}
	public COrder() {
		super();
	}


}
