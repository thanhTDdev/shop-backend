package com.devcamp.pizza365.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "car_type")
public class CCarType {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(name = "type_code", unique = true)
	private String typeCode;
	@Column(name = "type_name")
	private String typeName;
	@ManyToOne
	@JoinColumn(name = "car_id")
	@JsonIgnore
	private CCar car;
  
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_at" , nullable = true,updatable = false)
	@CreatedDate
	@CreationTimestamp
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date createAt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_at" , nullable = true)
	@LastModifiedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date updateAt;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public CCar getCar() {
		return car;
	}

	public void setCar(CCar car) {
		this.car = car;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public CCarType(int id, String typeCode, String typeName, CCar car, Date createAt, Date updateAt) {
		super();
		this.id = id;
		this.typeCode = typeCode;
		this.typeName = typeName;
		this.car = car;
		this.createAt = createAt;
		this.updateAt = updateAt;
	}

	public CCarType() {
	
	}


}
