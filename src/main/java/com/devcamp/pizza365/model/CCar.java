package com.devcamp.pizza365.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "car")
public class CCar {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(name = "car_code", unique = true)
	private String carCode;
	@Column(name = "car_name")
	private String carName;
	@OneToMany(mappedBy = "car", cascade = CascadeType.ALL)
    @JsonManagedReference
	private Set<CCarType> types ;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_at" , nullable = true,updatable = false)
	@CreatedDate
	@CreationTimestamp
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date createAt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_at" , nullable = true)
	@LastModifiedDate
	@JsonFormat(pattern = "dd-MM-yyyy")
	private Date updateAt;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCarCode() {
		return carCode;
	}

	public void setCarCode(String carCode) {
		this.carCode = carCode;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public Set<CCarType> getTypes() {
		return types;
	}

	public void setTypes(Set<CCarType> types) {
		this.types = types;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public CCar(long id, String carCode, String carName, Set<CCarType> types, Date createAt, Date updateAt) {
		super();
		this.id = id;
		this.carCode = carCode;
		this.carName = carName;
		this.types = types;
		this.createAt = createAt;
		this.updateAt = updateAt;
	}

	public CCar() {
		super();
	}


	
}
