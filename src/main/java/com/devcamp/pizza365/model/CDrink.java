package com.devcamp.pizza365.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "drinks")
public class CDrink {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
private long id;
	@Column(name = "ma_nuoc_uong")	
private String maNuocUong;
	@Column(name = "ten_nuoc_uong")	
private String tenNuocuong;
	@Column(name = "don_gia")	
private int donGia;
	@Column(name = "ngay_tao")	
private Date ngayTao;
	@Column(name = "ngay_cap_nhat")	
private Date ngayCapNhat;


public String getMaNuocUong() {
	return maNuocUong;
}
public void setMaNuocUong(String maNuocUong) {
	this.maNuocUong = maNuocUong;
}
public String getTenNuocuong() {
	return tenNuocuong;
}
public void setTenNuocuong(String tenNuocuong) {
	this.tenNuocuong = tenNuocuong;
}
public int getDonGia() {
	return donGia;
}
public void setDonGia(int donGia) {
	this.donGia = donGia;
}
public Date getNgayTao() {
	return ngayTao;
}
public void setNgayTao(Date ngayTao) {
	this.ngayTao = ngayTao;
}
public Date getNgayCapNhat() {
	return ngayCapNhat;
}
public void setNgayCapNhat(Date ngayCapNhat) {
	this.ngayCapNhat = ngayCapNhat;
}
public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public CDrink(long id , String maNuocUong, String tenNuocuong, int donGia, Date ngayTao, Date ngayCapNhat) {
	this.id = id;
	this.maNuocUong = maNuocUong;
	this.tenNuocuong = tenNuocuong;
	this.donGia = donGia;
	this.ngayTao = ngayTao;
	this.ngayCapNhat = ngayCapNhat;
}
public CDrink() {
}

}
