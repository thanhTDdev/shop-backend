package com.devcamp.pizza365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.web.bind.annotation.CrossOrigin;

@Entity
@Table(name = "products")
@CrossOrigin
public class Products {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
 private int id;
	@Column(name = "product_name")
 private String productName;
	@Column(name = "description_img")
 private  String descriptionImg; 
	@Column(name = "product_img")
 private String productImg ;
	@Column(name = "type")
 private String type ;
	@Column(name = "color")
 private String color;
	@Column(name = "price")
 private int price;
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getDescriptionImg() {
		return descriptionImg;
	}
	public void setDescriptionImg(String descriptionImg) {
		this.descriptionImg = descriptionImg;
	}
	public String getProductImg() {
		return productImg;
	}
	public void setProductImg(String productImg) {
		this.productImg = productImg;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public Products(int id, String productName, String descriptionImg, String productImg, String type, String color,
			int price) {
		super();
		this.id = id;
		this.productName = productName;
		this.descriptionImg = descriptionImg;
		this.productImg = productImg;
		this.type = type;
		this.color = color;
		this.price = price;
	}
	public Products() {
		super();
	}
	
}
