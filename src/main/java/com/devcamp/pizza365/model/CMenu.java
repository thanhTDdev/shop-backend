package com.devcamp.pizza365.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="menu"  )
public class CMenu {
	@Id
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long id ;
	@Column(name = "size")
	private String size;
	@Column(name = "duong_kinh")
	private String duongKinh;
	@Column(name = "suon_nuong")
	private String suongNuong;
	@Column(name = "salad")
	private String salad;
	@Column(name = "nuoc_ngot")
	private String nuocNgot;
	@Column(name = "gia_tien")
	private int giatien;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getDuongKinh() {
		return duongKinh;
	}
	public void setDuongKinh(String duongKinh) {
		this.duongKinh = duongKinh;
	}
	public String getSuongNuong() {
		return suongNuong;
	}
	public void setSuongNuong(String suongNuong) {
		this.suongNuong = suongNuong;
	}
	public String getSalad() {
		return salad;
	}
	public void setSalad(String salad) {
		this.salad = salad;
	}
	public String getNuocNgot() {
		return nuocNgot;
	}
	public void setNuocNgot(String nuocNgot) {
		this.nuocNgot = nuocNgot;
	}
	public int getGiatien() {
		return giatien;
	}
	public void setGiatien(int giatien) {
		this.giatien = giatien;
	}
	public CMenu(long id, String size, String duongKinh, String suongNuong, String salad, String nuocNgot,
			int giatien) {
		super();
		this.id = id;
		this.size = size;
		this.duongKinh = duongKinh;
		this.suongNuong = suongNuong;
		this.salad = salad;
		this.nuocNgot = nuocNgot;
		this.giatien = giatien;
	}
	public CMenu() {
		super();
	}

}
