package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.model.CCar;



public interface ICarRepository extends JpaRepository<CCar, Long> {
 CCar findAllTypeByCarCode(String carCode);
}
