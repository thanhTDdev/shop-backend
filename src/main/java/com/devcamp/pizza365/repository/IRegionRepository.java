package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.model.CRegion;



public interface IRegionRepository extends JpaRepository<CRegion, Long> {

}
