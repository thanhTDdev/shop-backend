package com.devcamp.pizza365.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.pizza365.model.Products;

public interface IProductRepository extends JpaRepository<Products, Long> {
	@Query(value = "SELECT * FROM products LIMIT :plimit OFFSET :poffset  ",nativeQuery = true)
	List<Products> findProductByProductsLikePageNumber(@Param("plimit") int limit ,@Param("poffset") int offset);
}
