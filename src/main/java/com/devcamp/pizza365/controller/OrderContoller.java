package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.COrder;
import com.devcamp.pizza365.repository.IOrderRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class OrderContoller {
@Autowired
IOrderRepository pOrderRepository;
@GetMapping("/Orders")
public ResponseEntity<List<COrder>> getAllOrder() {
    try {
        List<COrder> pOrder = new ArrayList<COrder>();
        pOrderRepository.findAll().forEach(pOrder::add);
        return new ResponseEntity<>(pOrder, HttpStatus.OK);
    } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
}
