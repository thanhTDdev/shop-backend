package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.Products;
import com.devcamp.pizza365.repository.IProductRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductController {
	@Autowired
	private IProductRepository producRepo;
// get All products
	@GetMapping("/allProducts")
	public ResponseEntity<List<Products>> getAllProducts() {
        try {
            List<Products> pProduct = new ArrayList<Products>();
            producRepo.findAll().forEach(pProduct::add);
            return new ResponseEntity<>(pProduct, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
// get product by PageNumber	
	@GetMapping("/products/{limit}/{offset}")
	public List<Products> getProductByLimitOffset(@PathVariable("limit") int plimit, @PathVariable("offset") int poffset) {
	return producRepo.findProductByProductsLikePageNumber(plimit,poffset);
	}
}
