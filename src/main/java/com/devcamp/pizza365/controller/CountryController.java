package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CDrink;
import com.devcamp.pizza365.model.CMenu;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.model.CVoucher;
import com.devcamp.pizza365.repository.ICountryRepository;
import com.devcamp.pizza365.repository.IDrinkRepository;
import com.devcamp.pizza365.repository.IMenuRepository;
import com.devcamp.pizza365.repository.IRegionRepository;
import com.devcamp.pizza365.repository.IVoucherRepository;


@CrossOrigin
@RestController
@RequestMapping("/")
public class CountryController {
	@Autowired
    ICountryRepository pCountryRepository;
	
	@Autowired
    IRegionRepository pRegionRepository;
	
	@GetMapping("/devcamp-countries")
	public ResponseEntity<List<CCountry>> getAllVouchers() {
        try {
            List<CCountry> pCountries = new ArrayList<CCountry>();

            pCountryRepository.findAll().forEach(pCountries::add);

            return new ResponseEntity<>(pCountries, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
	@GetMapping("/regions")
	public ResponseEntity<Set<CRegion>> getRegionsByCountryCode(@RequestParam(value = "countryCode") String countryCode) {
        try {
            CCountry vCountry = pCountryRepository.findByCountryCode(countryCode);
            if(vCountry != null) {
            	return new ResponseEntity<>(vCountry.getRegions(), HttpStatus.OK);
            } else {
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
