package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CDrink;
import com.devcamp.pizza365.repository.IDrinkRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DrinkController {
	
	@Autowired
	IDrinkRepository pDrinkRepository;

	@CrossOrigin
	@GetMapping("/drinks")
	public ResponseEntity<List<CDrink>> getAllDrink() {
		List<CDrink> pDrinks = new ArrayList<CDrink>();
		pDrinkRepository.findAll().forEach(pDrinks::add);
		return new ResponseEntity<>(pDrinks, HttpStatus.OK);
	}
	
}
