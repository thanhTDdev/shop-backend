package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.devcamp.pizza365.model.CUser;
import com.devcamp.pizza365.repository.IUserRepository;
@CrossOrigin
@RestController
@RequestMapping("/")
public class UserController {
@Autowired
 IUserRepository pUserRepositoty;
@GetMapping("/users")
public ResponseEntity<List<CUser>> getAllUser() {
    try {
        List<CUser> pUser = new ArrayList<CUser>();

        pUserRepositoty.findAll().forEach(pUser::add);

        return new ResponseEntity<>(pUser, HttpStatus.OK);
    } catch (Exception e) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
}
