package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CVoucher;
import com.devcamp.pizza365.repository.IVoucherRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class VoucherController {
	@Autowired
	IVoucherRepository pVoucherRepository;
	@GetMapping("/vouchers")
	public ResponseEntity<List<CVoucher>> getAllVoucher() {
		List<CVoucher> pVoucher = new ArrayList<CVoucher>();
		pVoucherRepository.findAll().forEach(pVoucher::add);
		return new ResponseEntity<>(pVoucher, HttpStatus.OK);
	}
	@GetMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> getCVoucherById(@PathVariable("id") long id) {
		Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
			return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@PostMapping("/vouchers")
	public ResponseEntity<CVoucher> createCVoucher(@Valid @RequestBody CVoucher pVouchers) {
		try {		
			CVoucher newVoucher =  new CVoucher(pVouchers.getId(),pVouchers.getMaVoucher(),pVouchers.getPhanTramGiamGia(),pVouchers.getGhiChu(),pVouchers.getNgayTao(),pVouchers.getNgayCapNhat());
  CVoucher _vouchers = pVoucherRepository.save(newVoucher);
			return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}
	@PutMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> updateCVoucherById(@PathVariable("id") long id, @RequestBody CVoucher pVouchers) {
		Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
			CVoucher _vouchers = voucherData.get();
			_vouchers.setGhiChu(pVouchers.getGhiChu());
			_vouchers.setMaVoucher(pVouchers.getMaVoucher());
			_vouchers.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
			_vouchers.setNgayTao(pVouchers.getNgayTao());
			_vouchers.setNgayCapNhat(pVouchers.getNgayCapNhat());
			return new ResponseEntity<>(pVoucherRepository.save(_vouchers), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}

	@DeleteMapping("/vouchers/{id}")
	public ResponseEntity<CVoucher> deleteCVoucherById(@PathVariable("id") long id) {
		try {
			pVoucherRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

}
