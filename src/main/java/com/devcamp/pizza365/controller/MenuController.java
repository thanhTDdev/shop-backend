package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CMenu;
import com.devcamp.pizza365.repository.IMenuRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class MenuController {
	@Autowired
	IMenuRepository pMenuRepository;
	@GetMapping("/menu")
	public ResponseEntity<List<CMenu>> getAllMenu() {	
		List<CMenu> pMenu = new ArrayList<CMenu>();
		pMenuRepository.findAll().forEach(pMenu::add);
		return new ResponseEntity<>(pMenu, HttpStatus.OK);
	}

}
