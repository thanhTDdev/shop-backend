package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.CCar;
import com.devcamp.pizza365.model.CCarType;
import com.devcamp.pizza365.repository.ICarRepository;
import com.devcamp.pizza365.repository.ICarTypeRepository;



@CrossOrigin
@RestController
@RequestMapping("/")
public class CarController {
	@Autowired
	ICarRepository pCarRepository;
	@Autowired
	ICarTypeRepository pCarTypeRepository;
	
	@GetMapping("/cars")
	public ResponseEntity<List<CCar>> getCar() {
        try {
            List<CCar> pCar = new ArrayList<CCar>();

            pCarRepository.findAll().forEach(pCar::add);

            return new ResponseEntity<>(pCar, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	@GetMapping("/cartypes")
	public ResponseEntity<Set<CCarType>> getRegionsByCountryCode(@RequestParam(value = "carCode") String carCode) {
        try {
            CCar vCar = pCarRepository.findAllTypeByCarCode(carCode);
            
            if(vCar != null) {
            	return new ResponseEntity<>(vCar.getTypes(), HttpStatus.OK);
            } else {
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
